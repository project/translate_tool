<?php

/**
 * @file
 * Translate Tool Module.
 */
?>
# Development helper for translation management

When developing a multilingual site you might want to add new
translatable strings and their translations in an update or install
hook.

This module adds a service that can help you with that:

```php
<?php
/**
 * Sample code.
 */
function hook_update_8101(&$sandbox) {
  $tt = \Drupal::service('translate_tool');

  $tt->add('horse', 'da', 'hest');
  $tt->delete('horse');'
}
?>
```
Or if you prefer a procedural interface (update and install hooks are procedural anyway):

```php
<?php
/**
 * Sample code.
 */
function hook_update_8102(&$sandbox) {
  translate_tool_add('horse', 'da', 'hest');
  translate_tool_delete('horse');'
}
?>
```

You can also add a context:

```php
<?php
/**
 * Sample code.
 */
function hook_update_8103(&$sandbox) {
  $tt = \Drupal::service('translate_tool');

  $tt->add('horse', 'da', 'hest', 'my-context');
  $tt->delete('horse', 'my-context');'
}
?>
```

Using context in a procedural style:

```php
<?php
/**
 * Sample code.
 */
function hook_update_8104(&$sandbox) {
  translate_tool_add('horse', 'da', 'hest', 'my-context');
  translate_tool_delete('horse', 'my-context');'
}
?>
```

The default context is used if you don't specify one yourself.
