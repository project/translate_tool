<?php

namespace Drupal\translate_tool;

/**
 * @file
 * Development helper for programmatically adding and deleting translations.
 */

use Drupal\locale\SourceString;
use Drupal\locale\StringStorageInterface;

/**
 * Development helper for programmatically adding and deleting translations.
 */
class TranslateTool {

  /**
   * Locales storage service.
   *
   * @var \Drupal\locale\StringStorageInterface
   */
  protected $localeStorage;

  /**
   * Construct.
   *
   * @param \Drupal\locale\StringStorageInterface $localeStorage
   *   Locales storage service.
   */
  public function __construct(StringStorageInterface $localeStorage) {
    $this->localeStorage = $localeStorage;
  }

  /**
   * Helper to programmatically add a single translation string.
   *
   * @param string $source
   *   Source string.
   * @param string $langcode
   *   The language code.
   * @param string $translation
   *   Translated string.
   * @param string $context
   *   (optional) Translation context. Defaults to the default context
   *   (empty string).
   */
  public function add(string $source, string $langcode, string $translation, string $context = '') {
    $parameters = [
      'source' => $source,
      'context' => $context,
    ];

    $string = $this->localeStorage->findString($parameters);

    if (is_null($string)) {
      $string = new SourceString();
      $string->setString($source);
      $string->setStorage($this->localeStorage);
      $string->context = $context;
      $string->save();
    }
    // Create translation. If one already exists, it will be replaced.
    $translation = $this->localeStorage->createTranslation([
      'lid' => $string->lid,
      'language' => $langcode,
      'translation' => $translation,
    ]);
    $translation->save();
  }

  /**
   * Helper to programmatically delete a single translation string.
   *
   * @param string $source
   *   Source string.
   * @param string $context
   *   (optional) Translation context. Defaults to the default context
   *   (empty string).
   */
  public function delete(string $source, string $context = '') {
    $parameters = [
      'source' => $source,
      'context' => $context,
    ];

    $this->localeStorage->deleteStrings($parameters);
  }

}
